﻿using Castle.Core.Logging;

namespace UltricoBMI.Web.Infrastructure.Command
{
    /// <summary>
    /// Marker interface which tells command runner that it should inject Logger into class
    /// </summary>
    public interface INeedLogger
    {
        ILogger Logger { get; set; }
    }
}