﻿using NHibernate;

namespace UltricoBMI.Web.Infrastructure.Query
{
    public abstract class Query<TResult>
    {
        public abstract TResult Execute(ISession session);
    }
}