﻿using System;
using System.Web;

namespace UltricoBMI.Web.Infrastructure.Service
{
    public static class HttpContextHelper
    {
        /// <summary>
        /// Returns app path. In case we are running in IIS Express/Dev server it returns empty string
        /// </summary>
        public static string GetAppPath(HttpContextBase httpContext)
        {
            var path = httpContext.Request.ApplicationPath;
            if ("/".Equals(path))
            {
                path = String.Empty;
            }
            return path;
        }
    }
}
