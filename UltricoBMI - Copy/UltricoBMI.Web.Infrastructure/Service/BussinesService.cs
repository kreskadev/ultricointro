﻿using UltricoBMI.Web.Infrastructure.Query;

namespace UltricoBMI.Web.Infrastructure.Service
{
    public abstract class BusinessService
    {
        protected NHibernate.ISession Session { get; private set; }

        /// <summary>
        /// Construct new instance, expects NHibernate session to be injcected
        /// </summary>
        protected BusinessService(NHibernate.ISession session)
        {
            this.Session = session;
        }

        /// <summary>
        /// Executes query
        /// </summary>
        protected virtual TResult Query<TResult>(Query<TResult> queryToExecute)
        {
            return queryToExecute.Execute(Session);
        }
    }
}