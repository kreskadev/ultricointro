﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace UltricoBMI.Web.Infrastructure.Web
{
    public static class BootstrapValidationExtensions
    {
        private const string HiddenListItem = "<li style=\"display:none\"></li>";

        public static MvcHtmlString BootstrapValidationSummary(this HtmlHelper htmlHelper)
        {
            return BootstrapValidationSummary(htmlHelper, false, "Please correct these errors:");
        }

        public static MvcHtmlString BootstrapValidationSummary(this HtmlHelper htmlHelper, bool excludePropertyErrors, string message)
        {
            if (htmlHelper == null)
            {
                throw new ArgumentNullException("htmlHelper");
            }

            var formContext = GetFormContextForClientValidation(htmlHelper.ViewContext);
            if (htmlHelper.ViewData.ModelState.IsValid)
            {
                if (formContext == null)
                {
                    // No client side validation
                    return null;
                }
                // TODO: This isn't really about unobtrusive; can we fix up non-unobtrusive to get rid of this, too?
                if (htmlHelper.ViewContext.UnobtrusiveJavaScriptEnabled && excludePropertyErrors)
                {
                    // No client-side updates
                    return null;
                }
            }

            string messageSpan;
            if (!String.IsNullOrEmpty(message) && !htmlHelper.ViewContext.ViewData.ModelState.IsValid)
            {
                var spanTag = new TagBuilder("span");
                spanTag.MergeAttribute("id", "validationSummaryHeader");
                var strongText = new TagBuilder("strong");
                strongText.SetInnerText(message);
                spanTag.InnerHtml += strongText.ToString();
                messageSpan = spanTag.ToString(TagRenderMode.Normal) + Environment.NewLine;
            }
            else
            {
                messageSpan = null;
            }

            var htmlSummary = new StringBuilder();
            var unorderedList = new TagBuilder("ul");

            var modelStates = GetModelStateList(htmlHelper, excludePropertyErrors);

            foreach (var modelState in modelStates)
            {
                foreach (var modelError in modelState.Errors)
                {
                    var errorText = GetUserErrorMessageOrDefault(htmlHelper.ViewContext.HttpContext, modelError, null /* modelState */);
                    if (String.IsNullOrEmpty(errorText)) { continue; }
                    var listItem = new TagBuilder("li");
                    listItem.SetInnerText(errorText);
                    htmlSummary.AppendLine(listItem.ToString(TagRenderMode.Normal));
                }
            }

            var htmlSummaryIsEmpty = htmlSummary.Length == 0;

            if (htmlSummaryIsEmpty)
            {
                htmlSummary.AppendLine(HiddenListItem);
            }

            unorderedList.InnerHtml = htmlSummary.ToString();

            var divBuilder = new TagBuilder("div");
            if (!htmlSummaryIsEmpty) divBuilder.AddCssClass("alert alert-error");
            divBuilder.AddCssClass((htmlHelper.ViewData.ModelState.IsValid) ? HtmlHelper.ValidationSummaryValidCssClassName : HtmlHelper.ValidationSummaryCssClassName);
            divBuilder.InnerHtml = messageSpan + unorderedList.ToString(TagRenderMode.Normal);

            if (formContext != null)
            {
                if (htmlHelper.ViewContext.UnobtrusiveJavaScriptEnabled)
                {
                    if (!excludePropertyErrors)
                    {
                        // Only put errors in the validation summary if they're supposed to be included there
                        divBuilder.MergeAttribute("data-valmsg-summary", "true");
                    }
                }
                else
                {
                    // client val summaries need an ID
                    divBuilder.GenerateId("validationSummary");
                    formContext.ValidationSummaryId = divBuilder.Attributes["id"];
                    formContext.ReplaceValidationSummary = !excludePropertyErrors;
                }
            }
            return divBuilder.ToMvcHtmlString(TagRenderMode.Normal);
        }

        public static string GetUserErrorMessageOrDefault(HttpContextBase httpContext, ModelError error, ModelState modelState)
        {
            if (!string.IsNullOrEmpty(error.ErrorMessage))
                return error.ErrorMessage;
            if (modelState == null)
                return null;
            var str = modelState.Value != null ? modelState.Value.AttemptedValue : null;
            return string.Format(CultureInfo.CurrentCulture, GetInvalidPropertyValueResource(httpContext), new object[] { str });
        }


        private static string GetInvalidPropertyValueResource(HttpContextBase httpContext)
        {
            string str = null;
            if (!string.IsNullOrEmpty(ValidationExtensions.ResourceClassKey) && httpContext != null)
            {
                str = httpContext.GetGlobalResourceObject(ValidationExtensions.ResourceClassKey, "InvalidPropertyValue", CultureInfo.CurrentUICulture) as string;
            }
            return str ?? "The value '{0}' is invalid.";
        }

        private static MvcHtmlString ToMvcHtmlString(this TagBuilder tagBuilder, TagRenderMode renderMode)
        {
            return new MvcHtmlString(tagBuilder.ToString(renderMode));
        }

        private static FormContext GetFormContextForClientValidation(ViewContext ctx)
        {
            return !ctx.ClientValidationEnabled ? null : ctx.FormContext;
        }

        private static IEnumerable<ModelState> GetModelStateList(HtmlHelper htmlHelper, bool excludePropertyErrors)
        {
            if (excludePropertyErrors)
            {
                ModelState modelState;
                htmlHelper.ViewData.ModelState.TryGetValue(htmlHelper.ViewData.TemplateInfo.HtmlFieldPrefix, out modelState);
                return modelState == null ? new ModelState[0] : new[] { modelState };
            }

            var ordering = new Dictionary<string, int>();
            var modelMetadata1 = htmlHelper.ViewData.ModelMetadata;
            if (modelMetadata1 != null)
            {
                foreach (var modelMetadata2 in modelMetadata1.Properties)
                    ordering[modelMetadata2.PropertyName] = modelMetadata2.Order;
            }
            return htmlHelper.ViewData.ModelState.Select(kv => new {kv, name = kv.Key })
                                                 .OrderBy(param0 => ordering.GetOrDefault(param0.name, 10000))
                                                 .Select(param0 => param0.kv.Value);
        }
    }
    public static class DictionaryExtensions
    {
        public static TValue GetOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dict, TKey key, TValue @default)
        {
            TValue obj;
            return dict.TryGetValue(key, out obj) ? obj : @default;
        }
    }
}