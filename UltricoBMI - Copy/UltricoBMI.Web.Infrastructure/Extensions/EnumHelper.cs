﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using UltricoBMI.Web.Infrastructure.Exception;

namespace UltricoBMI.Web.Infrastructure.Extensions
{
    public static class EnumHelper
    {
        public static IEnumerable<SelectListItem> EnumToSelectListItems<TEnum>() where TEnum : struct
        {
            return from TEnum e in Enum.GetValues(typeof(TEnum))
                   select new SelectListItem { Value = e.ToString(), Text = e.TranslateEnum() };
        }

        public static IEnumerable<SelectListItem> EnumToSelectListItemsWithEmptyElement<TEnum>() where TEnum : struct
        {
            yield return new SelectListItem();
            foreach (var item in EnumToSelectListItems<TEnum>())
                yield return item;
        }

        public static TEnum ToEnumValue<TEnum>(this string stringValue) where TEnum : struct
        {
            TEnum result;
            if (!Enum.TryParse(stringValue, out result))
                throw new TechnicalException(string.Format("Unable to find member {0} in enum type {1}", stringValue, typeof(TEnum).Name));
            return result;
        }
    }
}