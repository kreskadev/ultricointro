﻿namespace UltricoBMI.Web.Infrastructure.Extensions
{
    public static class StringExtensionMethods
    {
        public static string FormatWith(this string template, params object[] data)
        {
            return string.IsNullOrEmpty(template) ? string.Empty : string.Format(template, data);
        }
    }
}