﻿using System.Globalization;
using System.Threading;
using System.Web;

namespace UltricoBMI.Web.Infrastructure.Localization
{
    public class LocaleCookie
    {
        public const string CookieName = "locale";

        public static HttpCookie Create(string cultureCode)
        {
            return new HttpCookie(CookieName, cultureCode);
        }

        public static HttpCookie GetFromRequestOrCreateDefault()
        {
            var cultureCookie = HttpContext.Current.Request.Cookies[CookieName];

            if (cultureCookie != null && !string.IsNullOrEmpty(cultureCookie.Value))
                return cultureCookie;

            cultureCookie = Create(Thread.CurrentThread.CurrentCulture.Name);
            HttpContext.Current.Response.SetCookie(cultureCookie);
            return cultureCookie;
        }

        public static CultureInfo GetCultureFromRequestOrDefault()
        {
            return CultureInfo.GetCultureInfo(GetFromRequestOrCreateDefault().Value);
        }
    }
}