﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using UltricoBMI.Domain.BMIs.Entities;
using UltricoBMI.Domain.BMIs.Enums;
using UltricoBMI.Web.Infrastructure.Extensions;
using UltricoBMI.Web.Resources;

namespace UltricoBMI.Web.Models.BMIs
{
    public class BMIViewModel
    {
        public Guid? BMIId { get; set; }

        [Required, MaxLength(50), Display(Name = "BMIForm_Name", ResourceType = typeof(Labels))]
        public string Name { get; set; }

        [Required, Display(Name = "BMIForm_Gender", ResourceType = typeof(Labels))]
        public Gender Gender { get; set; }

        [Required, Range(0.01f, 500.0f), Display(Name = "BMIForm_Height", ResourceType = typeof(Labels))]
        public decimal Height { get; set; }

        [Required, Range(0.01f, 500.0f), Display(Name = "BMIForm_Weight", ResourceType = typeof(Labels))]
        public decimal Weight { get; set; }

        [Required, DataType(DataType.Date), Display(Name = "BMIForm_BirthDate", ResourceType = typeof(Labels))]
        public DateTime BirthDate { get; set; }

        [Required, Display(Name = "BMIForm_UnitSystem", ResourceType = typeof(Labels))]
        public UnitType UnitType { get; set; }

        [Required, Display(Name = "BMIForm_BMIValue", ResourceType = typeof(Labels))]
        public decimal BMIValue { get; set; }

        [Required, Display(Name = "BMIForm_BMICategory", ResourceType = typeof(Labels))]
        public BMICategory BMICategory { get; set; }

        //[Required, Display(Name = "BMIForm_BMICategory", ResourceType = typeof(Labels))]
        public DateTime CreatedDate { get; set; }

        public List<SelectListItem> Genders
        {
            get { return EnumHelper.EnumToSelectListItems<Gender>().ToList(); }
        }

        public List<SelectListItem> UnitTypes
        {
            get { return EnumHelper.EnumToSelectListItems<UnitType>().ToList(); }
        }

        public bool IsNewBMI
        {
            get { return !this.BMIId.HasValue; }
        }

        public BMIViewModel()
            : this(new PersonData())
        {
        }

        public BMIViewModel(PersonData personData)
        {
            this.BMIId = personData.Id;
            this.Name = personData.Name;
            this.BirthDate = personData.BirthDate;
            this.Gender = personData.Gender;
            this.Height = personData.Height;
            this.Weight = personData.Weight;
            this.UnitType = personData.UnitType;
            this.BMIValue = personData.BMIValue;
            this.BMICategory = personData.BMICategory;
            this.CreatedDate = personData.CreatedDate;
        }
    }

    public static class BMIViewModelExtensions
    {
        public static PersonData ToModel(this BMIViewModel vm)
        {
            return new PersonData(vm.Name, vm.BirthDate, vm.Gender, vm.Height, vm.Weight, vm.UnitType);
        }
    }
}
