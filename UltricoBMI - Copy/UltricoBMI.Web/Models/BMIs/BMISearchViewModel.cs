﻿using System;
using System.Collections.Generic;
using System.Linq;
using UltricoBMI.Domain.BMIs.Enums;
using UltricoBMI.Domain.BMIs.Queries;

namespace UltricoBMI.Web.Models.BMIs
{
    public class BMISearchViewModel
    {
        public IEnumerable<BMIViewModel> Results { get; set; }
        public decimal? AverageResultsBMI
        {
            get { return this.Results.Any() ? (this.Results.Sum(x => x.BMIValue)/this.Results.Count()) : (decimal?) null; }
        }

        public string Name { get; set; }

        public Gender? Gender { get; set; }
        public UnitType? UnitType { get; set; }

        public decimal? HeightFrom { get; set; }
        public decimal? HeightTo { get; set; }

        public decimal? WeightFrom { get; set; }
        public decimal? WeightTo { get; set; }

        public DateTime? BirthDateFrom { get; set; }
        public DateTime? BirthDateTo { get; set; }

        public DateTime? CreatedDateFrom { get; set; }
        public DateTime? CreatedDateTo { get; set; }

        public BMISearchViewModel()
            : this(new List<BMIViewModel>())
        {

        }

        public BMISearchViewModel(IEnumerable<BMIViewModel> results = null)
        {
            this.Results = results;
        }

        public BMISearchCriteria ToModel()
        {
            return new BMISearchCriteria
            {
                Name = this.Name,
                Gender = this.Gender,
                BirthDateFrom = this.BirthDateFrom,
                BirthDateTo = this.BirthDateTo,
                HeightFrom = this.HeightFrom,
                HeightTo = this.HeightTo,
                WeightFrom = this.WeightFrom,
                WeightTo = this.WeightTo,
                UnitType = this.UnitType,
                CreatedDateFrom = this.CreatedDateFrom,
                CreatedDateTo = this.CreatedDateTo
            };
        }
    }
}