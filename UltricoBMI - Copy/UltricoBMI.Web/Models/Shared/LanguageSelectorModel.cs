﻿using System.Collections.Generic;
using System.Globalization;
using System.Threading;

namespace UltricoBMI.Web.Models.Shared
{
    public class LanguageSelectorModel
    {
        public IList<CultureInfo> SupportedCultures { get; set; }

        public CultureInfo CurrentCulture
        {
            get { return Thread.CurrentThread.CurrentUICulture; }
        }
    }
}