﻿function BMISearchController() {
    var self = this;

    self.init = function () {
        $('#BMISearchForm').on('submit', self.executeSearchBMIs);

    };
    
    self.executeSearchBMIs = function (e) {
        e.preventDefault();
        e.returnValue = false;
        var $form = $(e.target);
        $.post($form.attr('action'), $form.serialize(), function (data) {
            $('#bmiSearchResults').html(data);
        });
    };

}
