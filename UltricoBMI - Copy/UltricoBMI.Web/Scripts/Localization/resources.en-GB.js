﻿var Messages = Messages || { translations: [] };

Messages.translations["en-GB"] = {
    validationErrorsDivHeader: "Please correct the following errors:",
    yes: "Yes",
    no: "No",
    ok: "OK"
};