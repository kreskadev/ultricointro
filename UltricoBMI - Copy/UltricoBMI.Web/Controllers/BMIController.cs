﻿using System;
using System.Linq;
using System.Web.Mvc;
using UltricoBMI.Domain.BMIs.Commands;
using UltricoBMI.Domain.BMIs.Queries;
using UltricoBMI.Web.Infrastructure.Web;
using UltricoBMI.Web.Models.BMIs;

namespace UltricoBMI.Web.Controllers
{
    public class BMIController : BMIControllerBase
    {
        public ActionResult Index(BMISearchViewModel model)
        {
            return this.View("Search", model);
        }

        [HttpPost]
        public ActionResult Search(BMISearchViewModel model)
        {
            var query = new FindBMIQuery(model.ToModel());
            model.Results = this.Query(query).Select(x => new BMIViewModel(x));
            return this.PartialView("_BMISearchResults", model);
        }

        [AllowAnonymous]
        public ActionResult NewBMI(NewBMICommand bmiCommand = null)
        {
            return this.View(bmiCommand != null ? new BMIViewModel(bmiCommand.ToPersonData()) : new BMIViewModel());
        }

        [HttpPost, AllowAnonymous]
        public ActionResult Calculate(NewBMICommand bmiCommand)
        {
            if (!ModelState.IsValid)
            {
                return View(bmiCommand);
            }

            var cmdResult = this.ExecuteCommand(new NewBMICommandHandler(bmiCommand));

            return cmdResult.Success
                ? (ActionResult) this.RedirectToAction("Display", new { bmiId = cmdResult.Result })
                : this.View("NewBMI", new BMIViewModel(bmiCommand.ToPersonData()));
        }

        [AllowAnonymous]
        public ActionResult Display(Guid bmiId)
        {
            var result = new BMIViewModel(this.Query(new GetBMIQuery(bmiId)));
            return this.View(result);
        }
    }
}
