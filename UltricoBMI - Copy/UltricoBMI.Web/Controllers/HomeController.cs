﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UltricoBMI.Web.Infrastructure.Web;

namespace UltricoBMI.Web.Controllers
{
    public class HomeController : BMIControllerBase
    {
        [AllowAnonymous]
        public ActionResult Index()
        {
            //Attention("Attention 1");
            //Attention("Attention 2");

            //Success("Success 1");
            //Success("Success 2");

            //Information("Information 1");
            //Information("Information 2");

            //Error("Error 1");
            //Error("Error 2");

            return this.View();
        }
    }
}
