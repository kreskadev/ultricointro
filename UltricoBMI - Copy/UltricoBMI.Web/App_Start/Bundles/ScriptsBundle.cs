﻿using System.Web.Optimization;

namespace UltricoBMI.Web.App_Start.Bundles
{
    public static class ScriptsBundle
    {
        public static void AddScriptsBundle(BundleCollection bundles)
        {
            bundles.AddJQueryBundle();
            bundles.AddModernizerBundle();
            bundles.AddBootstrapBundle();

            bundles.AddApplicationBundle();
        }

        private static void AddApplicationBundle(this BundleCollection bundles)
        {
            var applicationBundle = new ScriptBundle(BundlePaths.Scripts.AllScripts);

            applicationBundle.Include(
                "~/Scripts/globalize.js",
                "~/Scripts/globalize.culture.en-GB.js",
                "~/Scripts/globalize.culture.pl-PL.js"
            )
            .IncludeDirectory("~/Scripts/Localization", "*.js", searchSubdirectories: false)
            .IncludeDirectory("~/Scripts/Application", "*.js", searchSubdirectories: false);

            bundles.Add(applicationBundle);
        }

        private static void AddJQueryBundle(this BundleCollection bundles)
        {
            var jQueryBundle = new ScriptBundle(BundlePaths.Scripts.JQuery);

            jQueryBundle.Include(
                "~/Scripts/jquery-{version}.js",
                "~/Scripts/jquery-ui-{version}.js",
                "~/Scripts/jquery.selectboxes.js",
                "~/Scripts/jquery.unobtrusive*",
                "~/Scripts/jquery.validate*",
                "~/Scripts/jquery.cookie.js",
                "~/Scripts/jquery-ui-i18n.js"
            );

            bundles.Add(jQueryBundle);
        }

        private static void AddModernizerBundle(this BundleCollection bundles)
        {
            var modernizerBundle = new ScriptBundle(BundlePaths.Scripts.Modernizer);
            modernizerBundle.Include("~/Scripts/modernizr-*");

            bundles.Add(modernizerBundle);
        }

        private static void AddBootstrapBundle(this BundleCollection bundles)
        {
            var bootstrapBundle = new ScriptBundle(BundlePaths.Scripts.Bootstrap);
            bootstrapBundle.Include("~/Scripts/bootstrap.js");

            bundles.Add(bootstrapBundle);
        }
    }
}