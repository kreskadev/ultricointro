﻿using System.Reflection;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using NHibernate.Cfg;
using UltricoBMI.Domain.Installers;
using UltricoBMI.Web.Infrastructure.Installers;

namespace UltricoBMI.Web.Installers
{
    public class NHibernateInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            var cfg = ConfigureNHibernate("UltricoBMI.Database", new[] { GetType().Assembly, new BMIInstaller().GetType().Assembly });

            container.Register(
                Component.For<NHibernate.ISessionFactory>().UsingFactoryMethod(
                    kernel => cfg.BuildSessionFactory()).LifestyleSingleton(),
                Component.For<NHibernate.ISession>().UsingFactoryMethod(kernel =>
                {
                    var session = kernel.Resolve<NHibernate.ISessionFactory>().OpenSession();
                    session.FlushMode = NHibernate.FlushMode.Commit;
                    return session;
                }).LifestylePerWebRequest()
                );
        }

        protected virtual Configuration ConfigureNHibernate(string connectionStringName, Assembly[] assembliesWithMappings)
        {
            var cfg = new Configuration();

            cfg.DataBaseIntegration(
                db =>
                {
                    db.Dialect<NHibernate.Dialect.MsSql2008Dialect>();
                    db.Driver<NHibernate.Driver.SqlClientDriver>();
                    db.ConnectionProvider<NHibernate.Connection.DriverConnectionProvider>();
                    db.BatchSize = 500;
                    db.IsolationLevel = System.Data.IsolationLevel.ReadCommitted;
                    db.LogSqlInConsole = false;
                    db.ConnectionStringName = connectionStringName;
                    db.Timeout = 30;/*seconds*/
                }
                );

            cfg.Proxy(p => p.ProxyFactoryFactory<NHibernate.Bytecode.DefaultProxyFactoryFactory>());

            cfg.Cache(c => c.UseQueryCache = false);

            foreach (var assembly in assembliesWithMappings)
                cfg.AddAssembly(assembly);

            return cfg;
        }
    }  
}