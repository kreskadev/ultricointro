using Castle.Windsor;
using UltricoBMI.Domain.BMIs.Enums;
using UltricoBMI.Domain.BMIs.Logic;

namespace UltricoBMI.Domain.BMIs.Factory
{
    public interface IBMIFactory
    {
        IBMIAlgorithm GetAlgorithm(Gender gender, UnitType unitType);
    }

    public class BMIFactory : IBMIFactory
    {
        private readonly IWindsorContainer _container;

        public BMIFactory(IWindsorContainer container)
        {
            this._container = container;
        }

        public IBMIAlgorithm GetAlgorithm(Gender gender, UnitType unitType)
        {
            var bmiStatistic = this._container.Resolve<IBMIStatistic>(gender.ToString());
            var bmiCalculation = this._container.Resolve<IBMICalculation>(unitType.ToString());
            return this._container.Resolve<IBMIAlgorithm>(new { bmiStatistic, bmiCalculation });
        }
    }
}