﻿using System;
using Castle.Windsor;
using NHibernate;
using UltricoBMI.Domain.BMIs.Entities;
using UltricoBMI.Domain.BMIs.Enums;
using UltricoBMI.Domain.BMIs.Services;
using UltricoBMI.Web.Infrastructure.Command;

namespace UltricoBMI.Domain.BMIs.Commands
{
    public class NewBMICommand
    {
        public string Name { get; set; }
        public Gender Gender { get; set; }
        public UnitType UnitType { get; set; }
        public decimal Height { get; set; }
        public decimal Weight { get; set; }
        public DateTime BirthDate { get; set; }
    }

    public class NewBMICommandHandler : CommandHandler<Guid>, INeedSession
    {
        public ISession Session { get; set; }
        private IBMIService _bmiService;
        private readonly NewBMICommand _command;

        public NewBMICommandHandler(NewBMICommand command)
        {
            this._command = command;
        }

        public override Guid Execute()
        {
            using (var tx = this.Session.BeginTransaction())
            {
                var personData = this._command.ToPersonData();
                personData.CalcualteBMI(this._bmiService);
            
                this.Session.Save(personData);
                tx.Commit();
                return personData.Id;
            }
        }

        public override void SetupDependencies(IWindsorContainer container)
        {
            this._bmiService = container.Resolve<IBMIService>();
        }
    }

    public static class NewBMICommandExtensions
    {
        public static PersonData ToPersonData(this NewBMICommand cmd)
        {
            return new PersonData(cmd.Name, cmd.BirthDate, cmd.Gender, cmd.Height, cmd.Weight, cmd.UnitType);
        }
    }
}