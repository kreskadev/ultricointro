﻿using NHibernate;
using UltricoBMI.Domain.BMIs.Entities;
using UltricoBMI.Domain.BMIs.Factory;
using UltricoBMI.Domain.BMIs.Logic;
using UltricoBMI.Web.Infrastructure.Service;

namespace UltricoBMI.Domain.BMIs.Services
{
    public interface IBMIService
    {
        BMIResult CalculateBMI(PersonData personData);
    }

    public class BMIService : BusinessService, IBMIService
    {
        private readonly IBMIFactory _factory;

        public BMIService(ISession session, IBMIFactory factory)
            : base(session)
        {
            this._factory = factory;
        }

        public BMIResult CalculateBMI(PersonData personData)
        {
            var algorithm = _factory.GetAlgorithm(personData.Gender, personData.UnitType);
            var result = algorithm.CalcualteBMI(personData.Weight, personData.Height);
            return result;
        }
    }
}
