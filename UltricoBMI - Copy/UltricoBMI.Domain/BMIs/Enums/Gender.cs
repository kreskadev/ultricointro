﻿using System.ComponentModel.DataAnnotations;
using UltricoBMI.Web.Resources;

namespace UltricoBMI.Domain.BMIs.Enums
{
    public enum Gender
    {
        [Display(Name = "Gender_Male", ResourceType = typeof(Labels))]
        Male,
        [Display(Name = "Gender_Female", ResourceType = typeof(Labels))]
        Female,
        [Display(Name = "Gender_Child", ResourceType = typeof(Labels))]
        Child
    }
}