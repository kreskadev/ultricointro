﻿using UltricoBMI.Domain.BMIs.Enums;

namespace UltricoBMI.Domain.BMIs.Logic
{
    public class BMIResult
    {
        public BMICategory BMICategory { get; set; }
        public decimal BMIValue { get; set; }
    }

    public interface IBMIAlgorithm
    {
        BMIResult CalcualteBMI(decimal weight, decimal height);
    }

    public class BMIAlgorihtm : IBMIAlgorithm
    {
        private readonly IBMICalculation _bmiCalculation;
        private readonly IBMIStatistic _bmiStatistic;

        public BMIAlgorihtm(IBMIStatistic bmiStatistic, IBMICalculation bmiCalculation)
        {
            this._bmiCalculation = bmiCalculation;
            this._bmiStatistic = bmiStatistic;
        }

        public BMIResult CalcualteBMI(decimal weight, decimal height)
        {
            var bmiValue = this._bmiCalculation.CalculateBMIRounded(weight, height);
            var categoryResult = this._bmiStatistic.GetBMIInfo(bmiValue);

            return new BMIResult
            {
                BMIValue = bmiValue,
                BMICategory = categoryResult,
            };
        }
    }
}