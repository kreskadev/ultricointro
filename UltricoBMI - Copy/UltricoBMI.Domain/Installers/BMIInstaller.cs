using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using UltricoBMI.Domain.BMIs.Enums;
using UltricoBMI.Domain.BMIs.Factory;
using UltricoBMI.Domain.BMIs.Logic;
using UltricoBMI.Domain.BMIs.Services;

namespace UltricoBMI.Domain.Installers
{
    public class BMIInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            InstallStatistics(container);
            InstallCalculations(container);

            container.Register(Component.For<IBMIAlgorithm>()
                                        .ImplementedBy<BMIAlgorihtm>()
                                        .LifestyleTransient()
            );

            container.Register(Component.For<IBMIFactory>()
                                        .ImplementedBy<BMIFactory>()
                                        .LifestyleTransient()
            );

            container.Register(Component.For<IBMIService>()
                                        .ImplementedBy<BMIService>()
                                        .LifestyleTransient()
            );
        }

        private static void InstallStatistics(IWindsorContainer container)
        {
            container.Register(Component.For<IBMIStatistic>()
                                        .ImplementedBy<BMIChildStatistic>()
                                        .LifestyleTransient()
                                        .Named(Gender.Child.ToString())
            );
            container.Register(Component.For<IBMIStatistic>()
                                        .ImplementedBy<BMIFemaleStatistic>()
                                        .LifestyleTransient()
                                        .Named(Gender.Female.ToString())
            );
            container.Register(Component.For<IBMIStatistic>()
                                        .ImplementedBy<BMIMaleStatistic>()
                                        .LifestyleTransient()
                                        .Named(Gender.Male.ToString())
            );
        }

        private static void InstallCalculations(IWindsorContainer container)
        {
            container.Register(Component.For<IBMICalculation>()
                                        .ImplementedBy<BMIMetricCalculation>()
                                        .LifestyleTransient()
                                        .Named(UnitType.Metric.ToString())
            );
            container.Register(Component.For<IBMICalculation>()
                                        .ImplementedBy<BMIUSCalculation>()
                                        .LifestyleTransient()
                                        .Named(UnitType.US.ToString())
            );
        }
    }
}