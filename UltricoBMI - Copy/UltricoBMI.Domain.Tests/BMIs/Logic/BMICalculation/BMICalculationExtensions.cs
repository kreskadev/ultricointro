﻿using NUnit.Framework;
using UltricoBMI.Domain.BMIs.Logic;

namespace UltricoBMI.Domain.Tests.BMIs.Logic.BMICalculation
{
    [TestFixture]
    public class BMICalculationExtensionsTest
    {
        public decimal Execute(decimal weight, decimal height)
        {
            var calculation = new BMIMetricCalculation();

            return calculation.CalculateBMIRounded(weight, height);
        }

        [Test]
        public void Calculate()
        {
            var result = this.Execute(100, 180);

            Assert.AreEqual(result, 30.86M);
        }
    }
}
