﻿using System;

namespace UltricoBMI.Domain.Common
{
    public class Entity<TKey>
        where TKey : IComparable<TKey>, IEquatable<TKey>
    {
        public virtual TKey Id { get; set; }

        public virtual bool Equals(Entity<TKey> other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }
            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return this.Id.CompareTo(default(TKey)) != 0 && other.Id.CompareTo(default(TKey)) != 0 && other.Id.Equals(this.Id);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }
            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            return this.Equals(obj as Entity<TKey>);
        }

        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }
    }
}