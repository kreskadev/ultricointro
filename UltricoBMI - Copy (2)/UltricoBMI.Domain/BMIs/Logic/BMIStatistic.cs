﻿using UltricoBMI.Domain.BMIs.Enums;

namespace UltricoBMI.Domain.BMIs.Logic
{
    public interface IBMIStatistic
    {
        Gender Gender { get; }
        decimal NormalRangeFrom { get; }
        decimal NormalRangeTo { get; }

        BMICategory GetBMIInfo(decimal bmiValue);
    }

    public abstract class BMIBaseStatistic : IBMIStatistic
    {
        public abstract Gender Gender { get; }
        public abstract decimal NormalRangeFrom { get; }
        public abstract decimal NormalRangeTo { get; }

        public BMICategory GetBMIInfo(decimal bmiValue)
        {
            if (bmiValue < this.NormalRangeFrom)
            {
                return BMICategory.Underweight;
            }

            if (this.NormalRangeTo <= bmiValue)
            {
                return BMICategory.Overweight;
            }

            return BMICategory.Normal;
        }
    }

    public class BMIMaleStatistic : BMIBaseStatistic
    {
        public override Gender Gender { get { return Gender.Male; } }
        public override decimal NormalRangeFrom { get { return 18.5m; } }
        public override decimal NormalRangeTo { get { return 25.0m; } }
    }

    public class BMIFemaleStatistic : BMIBaseStatistic
    {
        public override Gender Gender { get { return Gender.Female; } }
        public override decimal NormalRangeFrom { get { return 19.5m; } }
        public override decimal NormalRangeTo { get { return 27.0m; } }
    }

    public class BMIChildStatistic : BMIBaseStatistic
    {
        public override Gender Gender { get { return Gender.Child; } }
        public override decimal NormalRangeFrom { get { return 20.0m; } }
        public override decimal NormalRangeTo { get { return 28.0m; } }
    }
}