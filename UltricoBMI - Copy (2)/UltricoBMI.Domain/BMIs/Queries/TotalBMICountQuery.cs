﻿using NHibernate;
using NHibernate.Criterion;
using UltricoBMI.Domain.BMIs.Entities;
using UltricoBMI.Web.Infrastructure.Query;

namespace UltricoBMI.Domain.BMIs.Queries
{
    public class TotalBMICountQuery : Query<int>
    {
        public override int Execute(ISession session)
        {
            return session.QueryOver<PersonData>().Select(Projections.RowCount()).FutureValue<int>().Value;
        }
    }
}
