﻿using System.ComponentModel.DataAnnotations;
using UltricoBMI.Web.Resources;

namespace UltricoBMI.Domain.BMIs.Enums
{
    public enum BMICategory
    {
        [Display(Name = "BMICategoryResult_Underweight", ResourceType = typeof(Labels))]
        Underweight,
        [Display(Name = "BMICategoryResult_Normal", ResourceType = typeof(Labels))]
        Normal,
        [Display(Name = "BMICategoryResult_Overweight", ResourceType = typeof(Labels))]
        Overweight
    }
}