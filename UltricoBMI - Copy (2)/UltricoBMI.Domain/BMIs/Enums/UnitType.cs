﻿using System.ComponentModel.DataAnnotations;
using UltricoBMI.Web.Resources;

namespace UltricoBMI.Domain.BMIs.Enums
{
    public enum UnitType
    {
        [Display(Name = "UnitType_Metric", ResourceType = typeof(Labels))]
        Metric,
        [Display(Name = "UnitType_US", ResourceType = typeof(Labels))]
        US
    }
}