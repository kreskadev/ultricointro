﻿using Castle.Core;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using UltricoBMI.Web.Infrastructure.Security;

namespace UltricoBMI.Web.Installers
{
    public class SecurityInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For<ISecurityProvider>()
                         .ImplementedBy<SimpleMembershipSecurityProvider>()
                         .LifeStyle.Is(LifestyleType.Singleton)
            );
        }
    }
}