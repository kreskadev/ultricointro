﻿using Castle.MicroKernel.Registration;
using UltricoBMI.Web.Infrastructure.Service;

namespace UltricoBMI.Web.Installers
{
    public class BusinessServiceInstaller : IWindsorInstaller
    {
        public void Install(Castle.Windsor.IWindsorContainer container, Castle.MicroKernel.SubSystems.Configuration.IConfigurationStore store)
        {
            container.Register(
                Classes.FromThisAssembly()
                       .BasedOn<BusinessService>()
                       .WithServiceSelf()
                       .WithServiceAllInterfaces()
                       .LifestyleTransient()
            );
        }
    }
}