﻿using UltricoBMI.Web.Infrastructure.Security;

namespace UltricoBMI.Web
{
    public static class Security
    {
        public static bool IsUserAuthenticated()
        {
            return GetSecurityProvider().IsUserAuthenticated();
        }

        public static IAppUser CurrentUser
        {
            get { return GetSecurityProvider().CurrentUser; }
        }

        public static int CurrentUserId
        {
            get { return GetSecurityProvider().CurrentUserId; }
        }

        public static string CurrentUserName
        {
            get { return GetSecurityProvider().CurrentUserName; }
        }

        public static bool IsUserInRole(string role)
        {
            return GetSecurityProvider().IsUserInRole(role);
        }

        public static string[] GetRolesForUser()
        {
            return GetSecurityProvider().GetRolesForUser();
        }

        public static bool Login(string userName, string password, bool persistSecurityCookie = false)
        {
            return GetSecurityProvider().Login(userName, password, persistSecurityCookie);
        }

        public static void Logout()
        {
            GetSecurityProvider().Logout();
        }

        private static ISecurityProvider GetSecurityProvider()
        {
            return MvcApplication.Container.Resolve<ISecurityProvider>();
        }
    }
}