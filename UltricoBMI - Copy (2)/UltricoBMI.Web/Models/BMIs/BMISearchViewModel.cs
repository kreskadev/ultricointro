﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using UltricoBMI.Domain.BMIs.Enums;
using UltricoBMI.Domain.BMIs.Queries;
using UltricoBMI.Web.Infrastructure.Const;
using UltricoBMI.Web.Resources;

namespace UltricoBMI.Web.Models.BMIs
{
    public class BMISearchViewModel
    {
        [MaxLength(50)]
        [Display(Name = "BMIForm_Name", ResourceType = typeof(Labels))]
        public string Name { get; set; }

        [Display(Name = "BMIForm_Gender", ResourceType = typeof(Labels))]
        public Gender? Gender { get; set; }

        [Display(Name = "BMIForm_UnitSystem", ResourceType = typeof(Labels))]
        public UnitType? UnitType { get; set; }

        [RegularExpression(Regexs.DecimalNumber, ErrorMessageResourceName = "ErrorMSG_Height_Numbers", ErrorMessageResourceType = typeof(Labels))]
        [Range(typeof(Decimal), "1", "9999", ErrorMessageResourceName = "ErrorMSG_Height_Range", ErrorMessageResourceType = typeof(Labels))]
        [Display(Name = "BMIForm_Height", ResourceType = typeof(Labels))]
        public decimal? HeightFrom { get; set; }

        [RegularExpression(Regexs.DecimalNumber, ErrorMessageResourceName = "ErrorMSG_Height_Numbers", ErrorMessageResourceType = typeof(Labels))]
        [Range(typeof(Decimal), "1", "9999", ErrorMessageResourceName = "ErrorMSG_Height_Range", ErrorMessageResourceType = typeof(Labels))]
        public decimal? HeightTo { get; set; }

        [RegularExpression(Regexs.DecimalNumber, ErrorMessageResourceName = "ErrorMSG_Weight_Numbers", ErrorMessageResourceType = typeof(Labels))]
        [Range(typeof(Decimal), "1", "9999", ErrorMessageResourceName = "ErrorMSG_Weight_Range", ErrorMessageResourceType = typeof(Labels))]
        [Display(Name = "BMIForm_Weight", ResourceType = typeof(Labels))]
        public decimal? WeightFrom { get; set; }

        [RegularExpression(Regexs.DecimalNumber, ErrorMessageResourceName = "ErrorMSG_Weight_Numbers", ErrorMessageResourceType = typeof(Labels))]
        [Range(typeof(Decimal), "1", "9999", ErrorMessageResourceName = "ErrorMSG_Weight_Range", ErrorMessageResourceType = typeof(Labels))]
        public decimal? WeightTo { get; set; }

        [DataType(DataType.Date, ErrorMessageResourceName = "ErrorMSG_Incorrect_Date", ErrorMessageResourceType = typeof(Labels))]
        [Display(Name = "BMIForm_BirthDate", ResourceType = typeof(Labels))]
        public DateTime? BirthDateFrom { get; set; }

        [DataType(DataType.Date, ErrorMessageResourceName = "ErrorMSG_Incorrect_Date", ErrorMessageResourceType = typeof(Labels))]
        public DateTime? BirthDateTo { get; set; }

        [DataType(DataType.Date, ErrorMessageResourceName = "ErrorMSG_Incorrect_Date", ErrorMessageResourceType = typeof(Labels))]
        [Display(Name = "BMIForm_CreatedDate", ResourceType = typeof(Labels))]
        public DateTime? CreatedDateFrom { get; set; }

        [DataType(DataType.Date, ErrorMessageResourceName = "ErrorMSG_Incorrect_Date", ErrorMessageResourceType = typeof(Labels))]
        public DateTime? CreatedDateTo { get; set; }

        public int? Page { get; set; }
        public BMISortOrder SortOrder { get; set; }
        public int? TotalResultsCount { get; set; }
        public IEnumerable<BMIViewModel> Results { get; set; }

        private readonly IList<SelectListItem> _pages = new List<SelectListItem>();
        public IList<SelectListItem> PagesSelectList
        {
            get
            {
                if (this._pages.Count == 0)
                {
                    for (var i = 0; i < Math.Ceiling((double) this.TotalResultsCount / 3); i++)
                    {
                        _pages.Add(new SelectListItem
                        {
                            Text = (i + 1).ToString(CultureInfo.InvariantCulture),
                            Value = i.ToString(CultureInfo.InvariantCulture)
                        });
                    }
                }

                return this._pages;
            }
        }

        public double? AvgBMIValue { get; set; }

        public BMISearchViewModel() : this(new List<BMIViewModel>()) { }

        public BMISearchViewModel(IEnumerable<BMIViewModel> results = null)
        {
            this.Results = results ?? new List<BMIViewModel>();
        }

        public BMISearchCriteria ToModel()
        {
            return new BMISearchCriteria
            {
                Name = this.Name,
                Gender = this.Gender,
                BirthDateFrom = this.BirthDateFrom,
                BirthDateTo = this.BirthDateTo,
                HeightFrom = this.HeightFrom,
                HeightTo = this.HeightTo,
                WeightFrom = this.WeightFrom,
                WeightTo = this.WeightTo,
                UnitType = this.UnitType,
                CreatedDateFrom = this.CreatedDateFrom,
                CreatedDateTo = this.CreatedDateTo,
                Page = this.Page,
            };
        }

        public void SetResult(BMIPagedSearchResult queryResult)
        {
            this.Results = queryResult.Results.Select(x => new BMIViewModel(x)).ToList();
            this.TotalResultsCount = queryResult.TotalCount;
            this.AvgBMIValue = queryResult.AverageResultsBMI;

        }
    }
}