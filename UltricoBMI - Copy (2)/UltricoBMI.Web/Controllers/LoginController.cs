﻿using System.Web.Mvc;
using UltricoBMI.Web.Infrastructure.Extensions;
using UltricoBMI.Web.Infrastructure.Web;

namespace UltricoBMI.Web.Controllers
{
    public class LoginController : BMIControllerBase
    {
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            if (Security.IsUserAuthenticated())
            {
                return RedirectToAction("Index", "Home");
            }

            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(string login, string pwd, string returnUrl)
        {
            if (Security.Login(login, pwd))
            {
                return RedirectToLocal(returnUrl);
            }

            Error("Login for user {0} failed".FormatWith(login));
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        public ActionResult Logout()
        {
            Security.Logout();
            return RedirectToAction("Login", "Login");
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
    }
}
