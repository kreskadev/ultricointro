﻿using System.Web.Optimization;
using UltricoBMI.Web.App_Start.Bundles;

namespace UltricoBMI.Web.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            ScriptsBundle.AddScriptsBundle(bundles);
            CssBundle.AddStyleBundle(bundles);

            bundles.IgnoreList.Clear();
            bundles.IgnoreList.Ignore("*.intellisense.js");
            bundles.IgnoreList.Ignore("*-vsdoc.js");
            bundles.IgnoreList.Ignore("*.min.js");
            bundles.IgnoreList.Ignore("*.min.css");
        }
    }
}