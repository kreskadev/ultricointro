﻿namespace UltricoBMI.Web.App_Start.Bundles
{
    public static class BundlePaths
    {
        public static class Scripts
        {
            public const string AllScripts = "~/js/scripts/all";
            public const string JQuery = "~/js/jQuery/";//"~/js/scripts/all";
            public const string Modernizer = "~/js/modernizr";//"~/js/scripts/all";//
            public const string Bootstrap = "~/js/bootstrap";//"~/js/scripts/all";//
        }
        public static class CSS
        {
            public const string AllCSS = "~/css";
        }
    }
}