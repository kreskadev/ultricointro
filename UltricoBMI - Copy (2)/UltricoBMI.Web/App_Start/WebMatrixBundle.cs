﻿using System;
using System.Web.Security;
using WebMatrix.WebData;

namespace UltricoBMI.Web.App_Start
{
    public static class SimpleMembershipInitializer
    {
        public static void Initialize()
        {
            try
            {
                WebSecurity.InitializeDatabaseConnection("UltricoBMI.Database", "AppUser", "UserId", "UserName", true);
                SeedUsers();
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("The ASP.NET Simple Membership database could not be initialized. For more information, please see http://go.microsoft.com/fwlink/?LinkId=256588", ex);
            }
        }

        private static void SeedUsers()
        {
            if (!Roles.RoleExists("administrators"))
            {
                Roles.CreateRole("administrators");
            }

            if (!WebSecurity.UserExists("admin"))
            {
                WebSecurity.CreateUserAndAccount("admin", "admin", new { FirstName = "John", LastName = "Doe", Email = "jd@mail.com" });

                Roles.AddUserToRole("admin", "administrators");
            }
        }
    }
}