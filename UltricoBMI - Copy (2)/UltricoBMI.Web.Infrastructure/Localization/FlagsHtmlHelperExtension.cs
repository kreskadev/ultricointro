﻿using System.Globalization;
using System.Web.Mvc;
using UltricoBMI.Web.Infrastructure.Service;

namespace UltricoBMI.Web.Infrastructure.Localization
{
    public static class FlagsHtmlHelperExtension
    {
        /// <summary>
        /// Returns image tag of a flag for given language
        /// </summary>
        public static MvcHtmlString Flag(this HtmlHelper helper, CultureInfo ci)
        {
            var imgTabBuilder = new TagBuilder("img");
            imgTabBuilder.AddCssClass("flag");
            imgTabBuilder.MergeAttribute("src", CultureToImagePath(helper, ci));

            return MvcHtmlString.Create(imgTabBuilder.ToString(TagRenderMode.SelfClosing));
        }

        private static string CultureToImagePath(HtmlHelper helper, CultureInfo ci)
        {
            return string.Concat(
                HttpContextHelper.GetAppPath(helper.ViewContext.RequestContext.HttpContext),
                "/Content/images/flags/",
                ci.Name, ".png");
        }
    }
}