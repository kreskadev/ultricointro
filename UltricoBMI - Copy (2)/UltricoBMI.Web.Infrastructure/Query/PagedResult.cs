﻿using System.Collections.Generic;
using System.Globalization;
using System.Web.Mvc;

namespace UltricoBMI.Web.Infrastructure.Query
{
    public class PagedResult<TType>
    {
        public PagedResult() : this(new List<TType>(), 0) { }

        public PagedResult(IList<TType> results, int totalCount)
        {
            this.Results = results;
            this.PageCount = 3;
            this.TotalCount = totalCount;
        }

        /// <summary>
        /// Results
        /// </summary>
        public IList<TType> Results { get; set; }
        /// <summary>
        /// Number of current page
        /// </summary>
        public int Page { get; set; }
        /// <summary>
        /// Items on page
        /// </summary>
        public int PageCount { get; set; }
        /// <summary>
        /// TotalItems.
        /// </summary>
        public int TotalCount { get; set; }

        
    }
}
