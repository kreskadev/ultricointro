﻿using NHibernate;

namespace UltricoBMI.Web.Infrastructure.Query
{
    public static class NHibernateSessionQueryExtension
    {
        public static TResult Query<TResult>(this ISession session, Query<TResult> queryToRun)
        {
            return queryToRun.Execute(session);
        }
    }
}