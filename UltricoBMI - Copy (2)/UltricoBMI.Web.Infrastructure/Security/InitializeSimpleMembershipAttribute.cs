﻿//using System;
//using System.Threading;
//using System.Web.Mvc;
//using System.Web.Security;
//using WebMatrix.WebData;

//namespace UltricoBMI.Web.Infrastructure.Security
//{
//    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
//    public sealed class InitializeSimpleMembershipAttribute : ActionFilterAttribute
//    {
//        private static SimpleMembershipInitializer _initializer;
//        private static object _initializerLock = new object();
//        private static bool _isInitialized;

//        public override void OnActionExecuting(ActionExecutingContext filterContext)
//        {
//            // Ensure ASP.NET Simple Membership is initialized only once per app start
//            LazyInitializer.EnsureInitialized(ref _initializer, ref _isInitialized, ref _initializerLock);
//        }

//        private class SimpleMembershipInitializer
//        {
//            public SimpleMembershipInitializer()
//            {
//                try
//                {
//                    WebSecurity.InitializeDatabaseConnection("UltricoBMI.Database", "AppUser", "UserId", "UserName", true);

//                    SeedUsers();
//                }
//                catch (System.Exception ex)
//                {
//                    throw new InvalidOperationException("The ASP.NET Simple Membership database could not be initialized. For more information, please see http://go.microsoft.com/fwlink/?LinkId=256588", ex);
//                }
//            }

//            private void SeedUsers()
//            {
//                if (!Roles.RoleExists("administrators"))
//                {
//                    Roles.CreateRole("administrators");
//                }

//                if (!WebSecurity.UserExists("admin"))
//                {
//                    WebSecurity.CreateUserAndAccount("admin", "admin", new { FirstName = "John", LastName = "Doe", Email = "jd@mail.com" });

//                    Roles.AddUserToRole("admin", "administrators");
//                }
//            }
//        }
//    }
//}