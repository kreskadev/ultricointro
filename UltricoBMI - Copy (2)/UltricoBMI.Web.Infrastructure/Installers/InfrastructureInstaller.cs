﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using UltricoBMI.Web.Infrastructure.Command;

namespace UltricoBMI.Web.Infrastructure.Installers
{
    public class InfrastructureInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<CommandRunner>().ImplementedBy<CommandRunner>().LifestyleTransient());
        }
    }
}