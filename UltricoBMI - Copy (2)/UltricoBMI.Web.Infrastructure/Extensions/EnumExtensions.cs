﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Resources;
using System.Web.Mvc;
using UltricoBMI.Web.Infrastructure.Exception;

namespace UltricoBMI.Web.Infrastructure.Extensions
{
    public static class EnumExtensions
    {
        public static SelectList ToSelectList<TEnum>(this TEnum enumValue) where TEnum : struct
        {
            var values = from TEnum e in Enum.GetValues(typeof(TEnum))
                         select new { Id = e, Name = e.TranslateEnum() };

            return new SelectList(values, "Id", "Name", enumValue);
        }

        public static string TranslateEnum<TEnum>(this TEnum enumValue) where TEnum : struct
        {
            var stringValue = enumValue.ToString();
            var enumType = typeof(TEnum);
            var attribute = GetDisplayAttribute(enumType, enumValue);
            if (attribute == null)
                return stringValue;
            if (attribute.ResourceType == null)
                return attribute.Name;

            var resourceManager = new ResourceManager(attribute.ResourceType);
            return resourceManager.GetString(attribute.Name);
        }

        private static DisplayAttribute GetDisplayAttribute(Type enumType, object enumValue)
        {
            var stringValue = enumValue.ToString();
            var enumMembers = enumType.GetMember(enumValue.ToString());
            if (enumMembers == null || enumMembers.Count() != 1)
                throw new TechnicalException(string.Format("Unable to find member {0} in enum type {1}", stringValue, enumType.Name));

            var displayAttributes = enumMembers[0].GetCustomAttributes(typeof(DisplayAttribute), false);
            if (!displayAttributes.Any())
                return null;

            return (DisplayAttribute)displayAttributes[0];
        }
    }
}