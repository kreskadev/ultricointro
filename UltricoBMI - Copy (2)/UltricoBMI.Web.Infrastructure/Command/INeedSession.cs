﻿using NHibernate;

namespace UltricoBMI.Web.Infrastructure.Command
{
    /// <summary>
    /// Marker interface which tells command runner that it should inject NHibernate session into class
    /// </summary>
    public interface INeedSession
    {
        /// <summary>
        /// NHibernate session instance.
        /// </summary>
        ISession Session { get; set; }
    }
}