﻿using NUnit.Framework;
using UltricoBMI.Domain.BMIs.Logic;
using UltricoBMI.Web.Infrastructure.Exception;

namespace UltricoBMI.Domain.Tests.BMIs.Logic.BMICalculation
{
    [TestFixture]
    public class BMIMetricCalculationTest
    {
        public decimal Execute(decimal weight, decimal height)
        {
            var calcualtion = new BMIMetricCalculation();

            return calcualtion.CalculateBMI(weight, height);
        }

        [Test]
        public void CalculateBMI_Succed()
        {
            var result = this.Execute(100, 180);

            Assert.AreEqual(result, 30.864197530864197530864197531M);
        }

        [Test]
        public void CalculateBMI_NegativeNumbers_Throw()
        {
            Assert.Throws<BusinessException>(() => this.Execute(0, 100));
            Assert.Throws<BusinessException>(() => this.Execute(100, 0));
        }
    }
}
