﻿using System;

namespace GiftAidCalculator.TestConsole.Calculator
{
    public static class TaxCalculatorExtensions
    {
        public static decimal CalculatePriceWithTaxRounded(this ITaxCalculator calculator, decimal amount)
        {
            return Math.Round(calculator.CalculatePriceWithTax(amount), 2);
        }
    }
}
