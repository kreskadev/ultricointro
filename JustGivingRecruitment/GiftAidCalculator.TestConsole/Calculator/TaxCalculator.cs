﻿using System;
using GiftAidCalculator.TestConsole.ExternalDataStore;

namespace GiftAidCalculator.TestConsole.Calculator
{
    public interface ITaxCalculator
    {
        decimal CalculatePriceWithTax(decimal amount);
    }

    public class TaxCalculator : ITaxCalculator
    {
        protected ITaxDataStore TaxDataStore { get; set; }

        public virtual decimal TaxRatio { get { return this.Tax / (100 - this.Tax); } }
        public decimal Tax { get { return this.TaxDataStore.GetTaxValue(); } }

        public TaxCalculator(ITaxDataStore taxDataStore)
        {
            this.TaxDataStore = taxDataStore;
        }

        public decimal CalculatePriceWithTax(decimal amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount");
            }

            return amount * this.TaxRatio;
        }
    }
}
