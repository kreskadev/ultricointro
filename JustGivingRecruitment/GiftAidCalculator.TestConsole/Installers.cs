﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using GiftAidCalculator.TestConsole.Calculator;
using GiftAidCalculator.TestConsole.Events;
using GiftAidCalculator.TestConsole.ExternalDataStore;

namespace GiftAidCalculator.TestConsole
{
    public class Installers : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            var regs = new IRegistration[]
            {
                Component.For<ITaxDataStore>().ImplementedBy<TaxDataStore>(),
                Component.For<ITaxCalculator>().ImplementedBy<TaxCalculator>(),
                Component.For<IAidEvent>().ImplementedBy<NormalEvent>().Named(AidEventType.Normal.ToString()),
                Component.For<IAidEvent>().ImplementedBy<SwimmingEvent>().Named(AidEventType.Swimming.ToString()),
                Component.For<IAidEvent>().ImplementedBy<RunningEvent>().Named(AidEventType.Running.ToString()),
            };

            container.Register(regs);
        }
    }
}
