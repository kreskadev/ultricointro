﻿using System;
using Castle.Windsor;
using Castle.Windsor.Installer;
using GiftAidCalculator.TestConsole.Events;
using GiftAidCalculator.TestConsole.Events.Extensions;

namespace GiftAidCalculator.TestConsole
{
    public class Program
    {
        private static WindsorContainer Container { get; set; }

        public static void Main(string[] args)
        {
            Application_Start();

            ExecuteProgram();

            Application_End();
        }

        private static void Application_Start()
        {
            InitializeWindsor();
        }

        private static void InitializeWindsor()
        {
            Container = new WindsorContainer();
            Container.Install(FromAssembly.This());
        }

        private static void Application_End()
        {
            if (Container != null)
            {
                Container.Dispose();
            }
        }

        private static void ExecuteProgram()
        {
            Console.WriteLine("Please Enter event name:");

            string input;
            AidEventType eventType;

            do
            {
                input = Console.ReadLine();
                if (Enum.TryParse(input, true, out eventType))
                {
                    break;
                }

                Console.WriteLine("Incorrect EventType");
                Console.WriteLine("Actual Events {0}|{1}|{2}",
                    AidEventType.Normal,
                    AidEventType.Running,
                    AidEventType.Swimming
                );
            } while (true);

            var aidEvent = Container.Resolve<IAidEvent>(eventType.ToString());

            Console.WriteLine("Please Enter donation amount:");
            input = Console.ReadLine();

            if (String.IsNullOrWhiteSpace(input))
            {
                Console.WriteLine("Invalid Input");
                Console.ReadLine();
                return;
            }

            Console.WriteLine(aidEvent.CalculateEffectiveDonorAmountRounded(decimal.Parse(input)));
            Console.WriteLine("Press any key to exit.");
            Console.ReadLine();
        }
    }
}
