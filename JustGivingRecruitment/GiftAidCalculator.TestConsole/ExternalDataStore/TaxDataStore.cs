﻿namespace GiftAidCalculator.TestConsole.ExternalDataStore
{
    public interface ITaxDataStore
    {
        decimal GetTaxValue();
    }

    public class TaxDataStore : ITaxDataStore
    {
        public decimal GetTaxValue()
        {
            return 20;
        }
    }
}
