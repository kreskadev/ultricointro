﻿using GiftAidCalculator.TestConsole.Calculator;

namespace GiftAidCalculator.TestConsole.Events
{
    public interface IAidEvent
    {
        decimal CalculateEffectiveDonorAmount(decimal amount);
    }

    public abstract class AidEventBase : IAidEvent
    {
        protected ITaxCalculator TaxCalculator { get; set; }

        protected AidEventBase(ITaxCalculator taxCalculator)
        {
            this.TaxCalculator = taxCalculator;
        }

        public abstract decimal CalculateEffectiveDonorAmount(decimal amount);
    }
}
