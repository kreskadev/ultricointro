﻿namespace GiftAidCalculator.TestConsole.Events
{
    public enum AidEventType
    {
        Normal,
        Swimming,
        Running
    }
}
