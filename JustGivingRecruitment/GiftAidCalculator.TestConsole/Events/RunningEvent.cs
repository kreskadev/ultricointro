﻿using GiftAidCalculator.TestConsole.Calculator;

namespace GiftAidCalculator.TestConsole.Events
{
    public class RunningEvent : AidEventBase
    {
        public RunningEvent(ITaxCalculator taxCalculator)
            : base(taxCalculator)
        {
        }

        public override decimal CalculateEffectiveDonorAmount(decimal amount)
        {
            return this.TaxCalculator.CalculatePriceWithTax(amount) * 1.05m;
        }
    }
}
