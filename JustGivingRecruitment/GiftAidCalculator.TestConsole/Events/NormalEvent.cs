﻿using GiftAidCalculator.TestConsole.Calculator;

namespace GiftAidCalculator.TestConsole.Events
{
    public class NormalEvent : AidEventBase
    {
        public NormalEvent(ITaxCalculator taxCalculator)
            : base(taxCalculator)
        {
        }

        public override decimal CalculateEffectiveDonorAmount(decimal amount)
        {
            return this.TaxCalculator.CalculatePriceWithTax(amount);
        }
    }
}