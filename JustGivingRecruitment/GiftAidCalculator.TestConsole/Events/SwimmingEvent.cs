﻿using GiftAidCalculator.TestConsole.Calculator;

namespace GiftAidCalculator.TestConsole.Events
{
    public class SwimmingEvent : AidEventBase
    {
        public SwimmingEvent(ITaxCalculator taxCalculator)
            : base(taxCalculator)
        {
        }

        public override decimal CalculateEffectiveDonorAmount(decimal amount)
        {
            return this.TaxCalculator.CalculatePriceWithTax(amount) * 1.03m;
        }
    }
}