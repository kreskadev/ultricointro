﻿using System;

namespace GiftAidCalculator.TestConsole.Events.Extensions
{
    public static class AidEventExtensions
    {
        public static decimal CalculateEffectiveDonorAmountRounded(this IAidEvent aidEvent, decimal amount)
        {
            return Math.Round(aidEvent.CalculateEffectiveDonorAmount(amount), 2);
        }
    }
}