﻿using FakeItEasy;
using GiftAidCalculator.TestConsole.Calculator;
using GiftAidCalculator.TestConsole.ExternalDataStore;
using NUnit.Framework;

namespace GiftAidCalculator.Tests.TaxCalculatorTest
{
    [TestFixture]
    public class TaxCalculatorExtensionsTests
    {
        public decimal Execute(decimal amount)
        {
            var dataStoreMock = A.Fake<ITaxDataStore>();
            A.CallTo(() => dataStoreMock.GetTaxValue()).Returns(7);

            var taxCalculator = new TaxCalculator(dataStoreMock);

            return taxCalculator.CalculatePriceWithTaxRounded(amount);
        }

        [Test]
        public void TaxCorrectNumber_RoundedResult()
        {
            const decimal amount = 99;

            var result = this.Execute(amount);

            Assert.AreEqual(result, 7.45m);
        }
    }
}
