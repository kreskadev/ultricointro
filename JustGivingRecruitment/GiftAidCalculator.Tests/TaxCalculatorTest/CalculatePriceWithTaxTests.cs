﻿using System;
using FakeItEasy;
using GiftAidCalculator.TestConsole.Calculator;
using GiftAidCalculator.TestConsole.ExternalDataStore;
using NUnit.Framework;

namespace GiftAidCalculator.Tests.TaxCalculatorTest
{
    [TestFixture]
    public class CalculatePriceWithTaxTests
    {
        public decimal Execute(decimal donorAmount)
        {
            var dataStoreMock = A.Fake<ITaxDataStore>();
            A.CallTo(() => dataStoreMock.GetTaxValue()).Returns(20);

            var taxCalculator = new TaxCalculator(dataStoreMock);

            return taxCalculator.CalculatePriceWithTax(donorAmount);
        }

        [Test]
        public void CalculatePriceWithTax_NegativePrice_Throws()
        {
            const decimal donorAmount = -100;

            Assert.Throws<ArgumentException>(() => this.Execute(donorAmount));
        }

        [Test]
        public void CalculatePriceWithTax_ZeroPrice_Throws()
        {
            const decimal donorAmount = 0;

            Assert.Throws<ArgumentException>(() => this.Execute(donorAmount));
        }


        [Test]
        public void CalculatePriceWithTax_CorrectAmount()
        {
            const decimal donorAmount = 100;

            var amountWithTax = this.Execute(donorAmount);

            Assert.AreEqual(amountWithTax, 25);
        }
    }
}
