﻿using FakeItEasy;
using GiftAidCalculator.TestConsole.Calculator;
using GiftAidCalculator.TestConsole.Events;
using NUnit.Framework;

namespace GiftAidCalculator.Tests.AidEvent
{
    [TestFixture]
    public class NormalEvent_CalculateEffectiveDonorAmount
    {
        public decimal Execute(decimal donorAmount)
        {
            var taxCalculatorMock = A.Fake<ITaxCalculator>();
            A.CallTo(() => taxCalculatorMock.CalculatePriceWithTax(A<decimal>._)).Returns(donorAmount);

            var aidEvent = new NormalEvent(taxCalculatorMock);

            return aidEvent.CalculateEffectiveDonorAmount(donorAmount);
        }

        [Test]
        public void CalculatePriceWithTax_NegativePrice_Throws()
        {
            const decimal donorAmount = 100;

            var result = this.Execute(donorAmount);

            Assert.AreEqual(result, 100);
        }
    }
}
