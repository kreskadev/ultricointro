﻿using FakeItEasy;
using GiftAidCalculator.TestConsole.Calculator;
using GiftAidCalculator.TestConsole.Events;
using NUnit.Framework;

namespace GiftAidCalculator.Tests.AidEvent
{
    [TestFixture]
    public class AidEventExtensionsTests
    {
        public decimal Execute(decimal donorAmount)
        {
            var taxCalculatorMock = A.Fake<ITaxCalculator>();
            A.CallTo(() => taxCalculatorMock.CalculatePriceWithTax(A<decimal>._)).Returns(donorAmount);

            var aidEvent = new SwimmingEvent(taxCalculatorMock);

            return aidEvent.CalculateEffectiveDonorAmount(donorAmount);
        }

        [Test]
        public void CalculatePriceWithTax_NegativePrice_Throws()
        {
            const decimal donorAmount = 999;

            var result = this.Execute(donorAmount);

            Assert.AreEqual(result, 1028.97m);
        }
    }
}
