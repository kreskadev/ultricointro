﻿namespace UltricoBMI.Web.Infrastructure.Security
{
    public interface IAppUser
    {
        int UserId { get; }
        string UserName { get; }
        string FirstName { get; }
        string LastName { get; }
        string Email { get; }
    }

    public class AppUser : IAppUser
    {
        public virtual int UserId { get; protected set; }
        public virtual string UserName { get; protected set; }
        public virtual string FirstName { get; protected set; }
        public virtual string LastName { get; protected set; }
        public virtual string Email { get; protected set; }

        protected AppUser()
        {

        }

        public AppUser(int userId, string userName, string firstName, string lastName, string email)
        {
            this.UserId = userId;
            this.UserName = userName;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Email = email;
        }
    }
}