﻿using System.Web.Security;
using UltricoBMI.Web.Resources;
using WebMatrix.Data;
using WebMatrix.WebData;

namespace UltricoBMI.Web.Infrastructure.Security
{
    public interface ISecurityProvider
    {
        IAppUser CurrentUser { get; }
        int CurrentUserId { get; }
        string CurrentUserName { get; }

        bool IsUserAuthenticated();
        bool IsUserInRole(string role);
        string[] GetRolesForUser();
        bool Login(string userName, string password, bool persistSecurityCookie = false);
        void Logout();
    }

    public class SimpleMembershipSecurityProvider : ISecurityProvider
    {
        public bool IsUserAuthenticated()
        {
            return WebSecurity.IsAuthenticated;
        }
        public IAppUser CurrentUser
        {
            get
            {
                //Query for user
                using (var db = Database.Open("UltricoBMI.Database"))
                {
                    var usr = db.QuerySingle("SELECT UserId, UserName, FirstName, LastName, Email FROM [dbo].[AppUser]  WHERE UserId = @0 ", this.CurrentUserId);
                    return new AppUser(this.CurrentUserId, this.CurrentUserName, usr.FirstName, usr.LastName, usr.Email);
                }
            }
        }
        public int CurrentUserId
        {
            get { return WebSecurity.CurrentUserId; }
        }

        public string CurrentUserName
        {
            get
            {
                var currentUserName = WebSecurity.CurrentUserName;
                return !string.IsNullOrWhiteSpace(currentUserName) ? currentUserName : DomainResources.AnonymousUserName;
            }
        }
        public bool IsUserInRole(string role)
        {
            return Roles.IsUserInRole(role);
        }

        public string[] GetRolesForUser()
        {
            return Roles.GetRolesForUser();
        }

        public bool Login(string userName, string password, bool persistSecurityCookie = false)
        {
            return WebSecurity.Login(userName, password, persistSecurityCookie);
        }

        public void Logout()
        {
            WebSecurity.Logout();
        }
    }
}