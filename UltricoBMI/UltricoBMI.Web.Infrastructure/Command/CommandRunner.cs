﻿using Castle.Core.Logging;
using Castle.Windsor;
using NHibernate;
using UltricoBMI.Web.Infrastructure.Exception;
using UltricoBMI.Web.Resources;

namespace UltricoBMI.Web.Infrastructure.Command
{
    public class CommandRunner
    {
        private readonly IWindsorContainer _windsor;
        private readonly ILogger _log;

        public CommandRunner(IWindsorContainer windsor)
        {
            this._windsor = windsor;
            this._log = this._windsor.Resolve<ILogger>();
        }

        public CommandExecutionResult<T> ExecuteCommand<T>(CommandHandler<T> cmd)
        {
            ProvideCommonServices(cmd);

            cmd.SetupDependencies(this._windsor);

            return InternalRun(cmd);
        }

        protected virtual void ProvideCommonServices<T>(CommandHandler<T> cmd)
        {
            if (cmd is INeedSession)
            {
                ((INeedSession)cmd).Session = this._windsor.Resolve<ISession>();
            }

            //inject logger
        }

        protected virtual CommandExecutionResult<T> InternalRun<T>(CommandHandler<T> cmd)
        {
            try
            {
                var result = cmd.Execute();

                return CommandExecutionResult<T>.SuccessResult(result);
            }
            catch (BusinessException businessEx)
            {
                LogCommandExecutionError(cmd, businessEx);
                return CommandExecutionResult<T>.FailureResult(businessEx.ErrorCode, businessEx.Message);
            }
            catch (SecurityException securityEx)
            {
                LogCommandExecutionError(cmd, securityEx);
                return CommandExecutionResult<T>.FailureResult(SecurityException.SecurityExceptionCode, securityEx.Message);
            }
            catch (System.Exception e)
            {
                LogCommandExecutionError(cmd, e);
                return CommandExecutionResult<T>.FailureResult("UnexpectedError", Labels.UnexpectedErrorDefaultMessage);
            }
        }

        protected void LogCommandExecutionError<T>(CommandHandler<T> cmd, System.Exception exception)
        {
            this._log.ErrorFormat("Execution of command: {0} failed. Error details below", cmd.GetType().ToString());
            this._log.Error(exception.Message, exception);
        }
    }
}