﻿using Castle.Windsor;
using UltricoBMI.Web.Infrastructure.Exception;
using UltricoBMI.Web.Infrastructure.Query;

namespace UltricoBMI.Web.Infrastructure.Command
{
    public interface ICommandHandler<out T>
    {
        T Execute();
        bool CanExecute();
    }

    public abstract class CommandHandler<T> : ICommandHandler<T>
    {
        public abstract T Execute();
        public abstract void SetupDependencies(IWindsorContainer container);

        public virtual bool CanExecute()
        {
            return true;
        }

        protected virtual TResult Query<TResult>(Query<TResult> queryToExecute)
        {
            var sessionProvider = this as INeedSession;
            if (sessionProvider == null)
                throw new TechnicalException("Command classes that require ability to execute queries must implement INeedSession interface.");
            return queryToExecute.Execute(sessionProvider.Session);
        }
    }

    public class CommandExecutionResult<T>
    {
        public bool Success { get; protected set; }
        public string ErrorCode { get; protected set; }
        public string MessageForHumans { get; protected set; }
        public T Result { get; protected set; }

        public static CommandExecutionResult<T> SuccessResult(T result)
        {
            return new CommandExecutionResult<T> { Success = true, Result = result };
        }

        public static CommandExecutionResult<T> FailureResult(string errCode, string msg)
        {
            return new CommandExecutionResult<T> { Success = false, ErrorCode = errCode, MessageForHumans = msg };
        }
    }
}