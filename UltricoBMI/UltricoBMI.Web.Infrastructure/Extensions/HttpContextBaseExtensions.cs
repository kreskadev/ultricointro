﻿using System;
using System.Web;

namespace UltricoBMI.Web.Infrastructure.Extensions
{
    public static class HttpContextExtensions
    {
        public static string GetAppPath(this HttpContextBase httpContext)
        {
            var path = httpContext.Request.ApplicationPath;
            return "/".Equals(path) ? String.Empty : path;
        }
    }
}