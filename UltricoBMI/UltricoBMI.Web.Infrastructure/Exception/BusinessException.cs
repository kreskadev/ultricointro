﻿using System;

namespace UltricoBMI.Web.Infrastructure.Exception
{
    public class BusinessException : ApplicationException
    {
        public string ErrorCode { get; protected set; }

        public BusinessException(string errorCode, string message)
            : base(message)
        {
            this.ErrorCode = errorCode;
        }

        public BusinessException(string errorCode, string messageTemplate, params object[] messageParameters)
            : base(string.Format(messageTemplate, messageParameters))
        {
            this.ErrorCode = errorCode;
        }
    }
}