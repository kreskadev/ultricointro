﻿using UltricoBMI.Web.Resources;

namespace UltricoBMI.Web.Infrastructure.Exception
{
    public class SecurityException : System.Exception
    {
        public const string SecurityExceptionCode = "SecException";

        public SecurityException() : base(Labels.SecurityExceptionDefaultMessage) { }

        public SecurityException(string message) : base(message) { }
    }
}