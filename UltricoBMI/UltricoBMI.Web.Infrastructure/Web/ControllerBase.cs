﻿using System.Collections.Generic;
using System.Web.Mvc;
using Castle.Windsor;
using NHibernate;
using UltricoBMI.Web.Infrastructure.Command;
using UltricoBMI.Web.Infrastructure.Query;
using UltricoBMI.Web.Infrastructure.Security;

namespace UltricoBMI.Web.Infrastructure.Web
{
    //[InitializeSimpleMembership]
    [Authorize]
    public class BMIControllerBase : Controller
    {
        /// <summary>
        /// Windsor container reference injected on controller creation
        /// </summary>
        public IWindsorContainer Container { get; set; }

        private CommandRunner _commandRunner;
        /// <summary>
        /// Command runner used internally to execute commands, resolved from container
        /// </summary>
        protected CommandRunner CommandRunner
        {
            get { return this._commandRunner ?? (this._commandRunner = this.Container.Resolve<CommandRunner>()); }
        }

        /// <summary>
        /// Executes command and provides generic processing of results
        /// </summary>
        protected CommandExecutionResult<T> ExecuteCommand<T>(CommandHandler<T> cmd)
        {
            var cmdResult = this.CommandRunner.ExecuteCommand(cmd);
            if (!cmdResult.Success)
            {
                Error(cmdResult.MessageForHumans);
            }
            return cmdResult;
        }

        /// <summary>
        /// Executes given query and returns results
        /// </summary>
        protected TResult Query<TResult>(Query<TResult> queryToRun)
        {
            return this.ProvideSession().Query(queryToRun);
        }

        /// <summary>
        /// Gives access to session
        /// </summary>
        protected ISession ProvideSession()
        {
            return this.Container.Resolve<ISession>();
        }

        private void AddMessage(string msgType, string msg)
        {
            if (TempData.ContainsKey(msgType))
            {
                ((List<string>)TempData[msgType]).Add(msg);
            }
            else
            {
                TempData[msgType] = new List<string> { msg };
            }
        }

        /// <summary>
        /// Causes bootstrap attention message to be rendered
        /// </summary>
        protected void Attention(string message)
        {
            this.AddMessage(Alerts.ATTENTION, message);
        }

        /// <summary>
        /// Causes bootstrap success message to be rendered
        /// </summary>
        protected void Success(string message)
        {
            this.AddMessage(Alerts.SUCCESS, message);
        }

        /// <summary>
        /// Causes bootstrap information message to be rendered
        /// </summary>
        protected void Information(string message)
        {
            this.AddMessage(Alerts.INFORMATION, message);
        }

        /// <summary>
        /// Causes bootstrap error message to be rendered
        /// </summary>
        protected void Error(string message)
        {
            this.AddMessage(Alerts.ERROR, message);
        }

        /// <summary>
        /// renders bootstrap error for all errors in model state
        /// </summary>
        protected void Error(ModelStateDictionary modelState)
        {
            var errMsg = new System.Text.StringBuilder();
            foreach (var m in ViewData.ModelState.Values)
            {
                foreach (var modelError in m.Errors)
                {
                    errMsg.AppendLine(modelError.ErrorMessage);
                }
            }
            this.AddMessage(Alerts.ERROR, errMsg.ToString());
        }
    }
}