﻿function BMISearchController() {
    var self = this;

    self.init = function () {
        $('#BMISearchForm').on('submit', self.executeSearchBMIs);
        $(document).on('change', '#Pagination', self.changePage);
    };
    
    self.executeSearchBMIs = function (e) {
        e.preventDefault();
        e.returnValue = false;
        var $form = $(e.target);
        $.post($form.attr('action'), $form.serialize(), function (data) {
            $('#bmiSearchResults').html(data);
        });
    };
    
    self.changePage = function () {
        $('#Page').value = $('#Pagination').val();
        $('#BMISearchForm').submit();
    };
}
