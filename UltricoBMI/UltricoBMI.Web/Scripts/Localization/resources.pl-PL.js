﻿var Messages = Messages || { translations: {} };

Messages.translations["pl-PL"] = {
    validationErrorsDivHeader: "Proszę poprawić następujące błędy:",
    yes: "Tak",
    no: "Nie",
    ok: "OK"
};