﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using UltricoBMI.Domain.BMIs.Entities;
using UltricoBMI.Domain.BMIs.Enums;
using UltricoBMI.Web.Infrastructure.Const;
using UltricoBMI.Web.Infrastructure.Extensions;
using UltricoBMI.Web.Resources;

namespace UltricoBMI.Web.Models.BMIs
{
    public class BMIViewModel
    {
        public Guid? BMIId { get; set; }

        [Required(ErrorMessageResourceName = "ErrorMSG_Name_Required", ErrorMessageResourceType = typeof(Labels))]
        [MaxLength(50)]
        [Display(Name = "BMIForm_Name", ResourceType = typeof(Labels))]
        public string Name { get; set; }

        [Required]
        [Display(Name = "BMIForm_Gender", ResourceType = typeof(Labels))]
        public Gender Gender { get; set; }

        [Required(ErrorMessageResourceName = "ErrorMSG_Height_Required", ErrorMessageResourceType = typeof(Labels))]
        [RegularExpression(Regexs.DecimalNumber, ErrorMessageResourceName = "ErrorMSG_Height_Numbers", ErrorMessageResourceType = typeof(Labels))]
        [Range(typeof(Decimal), "1", "9999", ErrorMessageResourceName = "ErrorMSG_Height_Range", ErrorMessageResourceType = typeof(Labels))]
        [Display(Name = "BMIForm_Height", ResourceType = typeof(Labels))]
        public decimal Height { get; set; }


        [Required(ErrorMessageResourceName = "ErrorMSG_Weight_Required", ErrorMessageResourceType = typeof(Labels))]
        [RegularExpression(Regexs.DecimalNumber, ErrorMessageResourceName = "ErrorMSG_Weight_Numbers", ErrorMessageResourceType = typeof(Labels))]
        [Range(typeof(Decimal), "1", "9999", ErrorMessageResourceName = "ErrorMSG_Weight_Range", ErrorMessageResourceType = typeof(Labels))]
        [Display(Name = "BMIForm_Weight", ResourceType = typeof(Labels))]
        public decimal Weight { get; set; }

        [Required(ErrorMessageResourceName = "ErrorMSG_Date_Required", ErrorMessageResourceType = typeof(Labels))]
        [DataType(DataType.Date, ErrorMessageResourceName = "ErrorMSG_Incorrect_Date", ErrorMessageResourceType = typeof(Labels))]
        [Display(Name = "BMIForm_BirthDate", ResourceType = typeof(Labels))]
        public DateTime BirthDate { get; set; }

        [Required, Display(Name = "BMIForm_UnitSystem", ResourceType = typeof(Labels))]
        public UnitType UnitType { get; set; }

        [Required, Display(Name = "BMIForm_BMIValue", ResourceType = typeof(Labels))]
        public decimal BMIValue { get; set; }

        [Required, Display(Name = "BMIForm_BMICategory", ResourceType = typeof(Labels))]
        public BMICategory BMICategory { get; set; }

        public DateTime CreatedDate { get; set; }

        public List<SelectListItem> Genders
        {
            get { return EnumHelper.EnumToSelectListItems<Gender>().ToList(); }
        }

        public List<SelectListItem> UnitTypes
        {
            get { return EnumHelper.EnumToSelectListItems<UnitType>().ToList(); }
        }

        public bool IsNewBMI
        {
            get { return !this.BMIId.HasValue; }
        }

        public BMIViewModel()
            : this(new PersonData())
        {
        }

        public BMIViewModel(PersonData personData)
        {
            this.BMIId = personData.Id;
            this.Name = personData.Name;
            this.BirthDate = personData.BirthDate;
            this.Gender = personData.Gender;
            this.Height = personData.Height;
            this.Weight = personData.Weight;
            this.UnitType = personData.UnitType;
            this.BMIValue = personData.BMIValue;
            this.BMICategory = personData.BMICategory;
            this.CreatedDate = personData.CreatedDate;
        }
    }

    public static class BMIViewModelExtensions
    {
        public static PersonData ToModel(this BMIViewModel vm)
        {
            return new PersonData(vm.Name, vm.BirthDate, vm.Gender, vm.Height, vm.Weight, vm.UnitType);
        }
    }
}
