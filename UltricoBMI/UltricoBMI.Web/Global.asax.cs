﻿using System;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Castle.Windsor.Installer;
using UltricoBMI.Domain.Installers;
using UltricoBMI.Web.App_Start;
using UltricoBMI.Web.Infrastructure.Installers;
using UltricoBMI.Web.Infrastructure.Localization;

namespace UltricoBMI.Web
{
    public class MvcApplication : HttpApplication
    {
        private static readonly IWindsorContainer _container = new WindsorContainer();

        /// <summary>
        /// Returns Windsor container instance
        /// </summary>
        public static IWindsorContainer Container
        {
            get { return _container; }
        }

        protected void Application_Start()
        {
            InitWindsor();
            ControllerBuilder.Current.SetControllerFactory(new WindsorControllerFactory(_container));

            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            SimpleMembershipInitializer.Initialize();
        }
        protected void Application_AcquireRequestState(object sender, EventArgs e)
        {
            var selectedCulture = LocaleCookie.GetCultureFromRequestOrDefault();
            Thread.CurrentThread.CurrentUICulture = selectedCulture;
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(selectedCulture.Name);
        }

        private static void InitWindsor()
        {
            Container.Register(Component.For<IWindsorContainer>().Instance(Container).LifestyleSingleton());
            Container.Install(FromAssembly.This());
            Container.Install(new InfrastructureInstaller());
            Container.Install(new BMIInstaller());
        }
    }
}