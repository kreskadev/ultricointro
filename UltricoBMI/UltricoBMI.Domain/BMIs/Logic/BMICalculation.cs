﻿using System;
using UltricoBMI.Domain.BMIs.Enums;
using UltricoBMI.Web.Infrastructure.Exception;

namespace UltricoBMI.Domain.BMIs.Logic
{
    public interface IBMICalculation
    {
        UnitType UnitType { get; }
        decimal CalculateBMI(decimal weight, decimal height);
    }

    public class BMIMetricCalculation : IBMICalculation
    {
        public UnitType UnitType { get { return UnitType.Metric; } }

        public decimal CalculateBMI(decimal weight, decimal height)
        {
            if (weight <= 0)
            {
                throw new BusinessException("Weight", "Weight cannot be less than 0");
            }

            if (height <= 0)
            {
                throw new BusinessException("Height", "Height cannot be less than 0");
            }

            return weight / (height * height);
        }
    }

    public class BMIUSCalculation : IBMICalculation
    {
        public UnitType UnitType { get { return UnitType.US; } }

        public decimal CalculateBMI(decimal weight, decimal height)
        {
            if (weight <= 0)
            {
                throw new BusinessException("Weight", "Weight cannot be less than 0");
            }

            if (height <= 0)
            {
                throw new BusinessException("Height", "Height cannot be less than 0");
            }

            return (weight / (height * height)) * 703;
        }
    }

    public static class BMICalculationExtensons
    {
        public static decimal CalculateBMIRounded(this IBMICalculation calculation, decimal weight, decimal height)
        {
            return Math.Round(calculation.CalculateBMI(weight, height), 2);
        }
    }
}