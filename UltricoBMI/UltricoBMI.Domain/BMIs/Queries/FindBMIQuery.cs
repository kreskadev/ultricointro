﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using UltricoBMI.Domain.BMIs.Entities;
using UltricoBMI.Domain.BMIs.Enums;
using UltricoBMI.Web.Infrastructure.Query;

namespace UltricoBMI.Domain.BMIs.Queries
{
    public abstract class PagedSearchCriteria
    {
        public int? Page { get; set; }
        public BMISortOrder SortOrder { get; set; }
    }

    public class BMISearchCriteria : PagedSearchCriteria
    {
        public string Name { get; set; }

        public Gender? Gender { get; set; }
        public UnitType? UnitType { get; set; }

        public decimal? HeightFrom { get; set; }
        public decimal? HeightTo { get; set; }

        public decimal? WeightFrom { get; set; }
        public decimal? WeightTo { get; set; }

        public DateTime? BirthDateFrom { get; set; }
        public DateTime? BirthDateTo { get; set; }

        public DateTime? RegisterDateFrom { get; set; }
        public DateTime? RegisterDateTo { get; set; }

        public DateTime? CreatedDateFrom { get; set; }
        public DateTime? CreatedDateTo { get; set; }
    }

    public enum BMISortOrder
    {
        Ascending,
        Descending
    }

    public class FindBMIQuery : Query<BMIPagedSearchResult>
    {
        private readonly BMISearchCriteria _searchCriteria;

        public FindBMIQuery(BMISearchCriteria criteria)
        {
            this._searchCriteria = criteria;
        }

        public override BMIPagedSearchResult Execute(ISession session)
        {
            var query = session.QueryOver<PersonData>();

            //Name
            if (!string.IsNullOrWhiteSpace(_searchCriteria.Name))
                query.Where(personData => personData.Name.IsLike(_searchCriteria.Name.ConvertWildcards()));

            //Birth date
            if (_searchCriteria.BirthDateFrom.HasValue)
                query.Where(personData => personData.BirthDate >= _searchCriteria.BirthDateFrom.Value);

            if (_searchCriteria.BirthDateTo.HasValue)
                query.Where(personData => personData.BirthDate <= _searchCriteria.BirthDateTo.Value);

            //Gender
            if (_searchCriteria.Gender.HasValue)
                query.Where(personData => personData.Gender <= _searchCriteria.Gender);

            //Height
            if (_searchCriteria.HeightFrom.HasValue)
                query.Where(personData => personData.Height >= _searchCriteria.HeightFrom);

            if (_searchCriteria.HeightTo.HasValue)
                query.Where(personData => personData.Height <= _searchCriteria.HeightTo);

            //Weight
            if (_searchCriteria.WeightFrom.HasValue)
                query.Where(personData => personData.Weight >= _searchCriteria.WeightFrom);

            if (_searchCriteria.WeightTo.HasValue)
                query.Where(personData => personData.Weight <= _searchCriteria.WeightTo);

            //UnitType
            if (_searchCriteria.UnitType.HasValue)
                query.Where(personData => personData.UnitType <= _searchCriteria.UnitType);

            //Created Date
            if (_searchCriteria.CreatedDateFrom.HasValue)
                query.Where(personData => personData.CreatedDate >= _searchCriteria.CreatedDateFrom);

            if (_searchCriteria.CreatedDateTo.HasValue)
                query.Where(personData => personData.CreatedDate <= _searchCriteria.CreatedDateTo);

            var avgQuery = query.Clone();

            var avgValue = avgQuery.SelectList(x => x.SelectAvg(m => m.BMIValue)).FutureValue<double>().Value;
            avgValue = Math.Round(avgValue, 2);

            //Pagination
            if (!this._searchCriteria.Page.HasValue)
            {
                this._searchCriteria.Page = 0;
            }
            
            query.Skip(this._searchCriteria.Page.Value*3).Take(3);

            var totalItemCount = new TotalBMICountQuery().Execute(session);

            var queryResutls = query.List();

            var result = new BMIPagedSearchResult(queryResutls, totalItemCount, avgValue)
            {
                TotalCount = totalItemCount,
                PageCount = 3,
            };

            return result;
        }
    }

    public class BMIPagedSearchResult : PagedResult<PersonData>
    {
        public BMIPagedSearchResult(IList<PersonData> searchResults, int totalCount, double avgValue)
            : base(searchResults, totalCount)
        {
            this.AverageResultsBMI = avgValue;
        }

        public double? AverageResultsBMI { get; set; }
    }

    public static class SqlWilcardStringExtensions
    {
        /// <summary>
        /// Converts wilcards for search from user friendly * to sql friendly %
        /// </summary>
        public static string ConvertWildcards(this string target)
        {
            if (string.IsNullOrWhiteSpace(target))
                return target;

            return target.Replace('*', '%');
        }
    }
}