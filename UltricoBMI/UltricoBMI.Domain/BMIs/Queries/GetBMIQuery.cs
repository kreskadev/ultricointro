﻿using System;
using NHibernate;
using UltricoBMI.Domain.BMIs.Entities;
using UltricoBMI.Web.Infrastructure.Query;

namespace UltricoBMI.Domain.BMIs.Queries
{
    public class GetBMIQuery : Query<PersonData>
    {
        private readonly Guid _searchedBmiId;

        public GetBMIQuery(Guid bmiId)
        {
            this._searchedBmiId = bmiId;
        }

        public override PersonData Execute(ISession session)
        {
            return session.Load<PersonData>(this._searchedBmiId);
        }
    }
}