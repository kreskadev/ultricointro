﻿using System;
using UltricoBMI.Domain.BMIs.Enums;
using UltricoBMI.Domain.BMIs.Services;
using UltricoBMI.Domain.Common;

namespace UltricoBMI.Domain.BMIs.Entities
{
    public class PersonData : Entity<Guid>
    {
        public virtual string Name { get; protected set; }
        public virtual Gender Gender { get; protected set; }

        public virtual decimal Height { get; protected set; }
        public virtual decimal Weight { get; protected set; }
        public virtual DateTime BirthDate { get; protected set; }
        public virtual UnitType UnitType { get; protected set; }

        public virtual decimal BMIValue { get; protected set; }
        public virtual BMICategory BMICategory { get; protected set; }

        public virtual DateTime CreatedDate { get; protected set; }

        public PersonData() { }

        public PersonData(string name, DateTime birthDate, Gender gender, decimal height, decimal weight, UnitType unitSystem)
        {
            this.Name = name;
            this.Gender = gender;
            this.Height = height;
            this.Weight = weight;
            this.BirthDate = birthDate;
            this.UnitType = unitSystem;
            
            this.CreatedDate = DateTime.Now;
        }

        protected internal virtual void CalcualteBMI(IBMIService bmiService)
        {
            var result = bmiService.CalculateBMI(this);

            this.BMIValue = result.BMIValue;
            this.BMICategory = result.BMICategory;
        }
    }
}