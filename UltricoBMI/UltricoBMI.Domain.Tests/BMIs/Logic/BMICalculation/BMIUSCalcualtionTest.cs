﻿using NUnit.Framework;
using UltricoBMI.Domain.BMIs.Logic;
using UltricoBMI.Web.Infrastructure.Exception;

namespace UltricoBMI.Domain.Tests.BMIs.Logic.BMICalculation
{
    [TestFixture]
    public class BMIUSCalcualtionTest
    {
        public decimal Execute(decimal weight, decimal height)
        {
            var calcualtion = new BMIUSCalculation();

            return calcualtion.CalculateBMI(weight, height);
        }

        [Test]
        public void CalculateBMI_Succed()
        {
            var result = this.Execute(100, 180);

            Assert.AreEqual(result, 2.1697530864197530864197531194M);
        }

        [Test]
        public void CalculateBMI_NegativeNumbers_Throw()
        {
            Assert.Throws<BusinessException>(() => this.Execute(0, 100));
            Assert.Throws<BusinessException>(() => this.Execute(100, 0));
        }
    }
}
