﻿using FakeItEasy;
using NUnit.Framework;
using UltricoBMI.Domain.BMIs.Enums;
using UltricoBMI.Domain.BMIs.Logic;

namespace UltricoBMI.Domain.Tests.BMIs.Logic.BMIStatistic
{
    [TestFixture]
    public class BMIStatisticTest
    {
        public BMICategory Execute(decimal bmiValue)
        {
            var fake = A.Fake<BMIBaseStatistic>();

            A.CallTo(() => fake.NormalRangeFrom).Returns(2);
            A.CallTo(() => fake.NormalRangeTo).Returns(4);

            return fake.GetBMIInfo(bmiValue);
        }

        [Test]
        public void NormalRange()
        {
            var result = this.Execute(3);
            Assert.AreEqual(result, BMICategory.Normal);
        }

        [Test]
        public void OverweightRange()
        {
            var result = this.Execute(5);
            Assert.AreEqual(result, BMICategory.Overweight);
        }

        [Test]
        public void UnderweightRange()
        {
            var result = this.Execute(1);
            Assert.AreEqual(result, BMICategory.Underweight);
        }
    }
}
